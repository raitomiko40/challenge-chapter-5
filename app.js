const express = require("express")
const app = express()
const port = 3000

app.use(express.static('public'))
app.use(express.static('assets'))
app.use(express.static('controller'))


app.set("view engine", "ejs")

app.get("/", (req, res) => {
    res.render("index")
})
app.get("/games", (req, res) => {
    res.render("games")
})
app.get("/user", (req, res) => {
    res.sendFile("./data/user.json", { root: __dirname})
})

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})
